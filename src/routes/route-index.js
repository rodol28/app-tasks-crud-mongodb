const router = require('express').Router();
const taskModel = require('../models/model-task');
const connection = require('../libs/db-connection')();

router.get('/', (req, res) => {
    taskModel.find({}, (err, tasks) => {
        if (err) throw err;
        res.render('../views/view-index', {
            tasks: tasks
        });
    });

});

router.post('/add-task', (req, res) => {
    let body = req.body;
    body.status = false;

    taskModel.create(body, (err, task) => {
        if(err) throw err;
        res.redirect('/');
    });
});

router.get('/turn/:id', (req, res) => {
    let id = req.params.id;
    taskModel.findById(id, (err, task) => {
        if(err) throw err;

        task.status = !task.status;
        task.save().then(() => {
            res.redirect('/');
        });
    });
});

router.get('/delete/:id', (req, res) => {
    let id = req.params.id;
    taskModel.remove({_id: id}, (err, result) => {
        if(err) throw err;
        res.redirect('/');
    });
});

module.exports = router;