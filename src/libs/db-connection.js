const mongoose = require('mongoose');

let db;

module.exports = function connection() {
    if (!db) {
        db = mongoose.connect('mongodb://localhost/crud-example', {
            useNewUrlParser: true,
            useUnifiedTopology: true
        }).then(() => console.log('db is connected')).catch((err) => console.log('db is not connected'));
    }
    return db;
};